/**
 * Created by Administrator on 2018/3/28 0028.
 */
class DangerException extends  Exception{
    final String message="超重";
    public  String warnMess() {
        return message;
    }
}
class CargoBoat {
    int realContent;
    int maxContent;
    public void setMaxContent(int c) {
        maxContent=c;
    }
    public  void loading(int m) throws  DangerException {
        realContent+=m;
        if(realContent>maxContent) {
            realContent-=m;
            throw new DangerException();
        }
        System.out.println("目前装载了"+realContent+"吨货物");
    }
}
public class p171 {
    public static void main(String[] args) {
        CargoBoat ship=new CargoBoat();
        ship.setMaxContent(1000);
        int m=600;
        try{
            ship.loading(m);
            m=400;
            ship.loading(m);
            m=367;
            ship.loading(m);
            m=555;
            ship.loading(m);
        }
        catch (DangerException e)
        {
            System.out.println(e.warnMess());
            System.out.println("无法再装载重量是"+m+"吨的集装箱");
        }
        finally {
            System.out.print("货船将正点起航");
        }
    }
}
