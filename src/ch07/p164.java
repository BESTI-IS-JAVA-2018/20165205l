import com.sun.org.apache.xpath.internal.SourceTree;

abstract class OutputAlphabet {
    public abstract void output();
}
class OutputEnglish extends OutputAlphabet {
    public  void output() {
        for (char c='a';c<='z';c++) {
            System.out.printf("%3c",c);
        }
    }
}
class ShowBoard {
    void showMess(OutputAlphabet show){
        show.output();
    }
}
public  class p164{
    public static void main(String[] args) {
        ShowBoard board=new ShowBoard();
        board.showMess(new OutputEnglish());
        board.showMess(new OutputAlphabet() {
            @Override
            public void output() {
              for (char c='α';c<='ω';c++)
                        System.out.printf("%3c",c);
            }
        });
    }
}