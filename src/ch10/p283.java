/**
 * Created by Administrator on 2018/3/28 0028.
 */
import com.sun.org.apache.xpath.internal.SourceTree;

import  java.io.*;
class FileAccept implements FilenameFilter {
    private String extendName;
    public void setExtendName(String s) {
        extendName="."+s;
    }
    public  boolean accept(File dir,String name){
        return name.endsWith(extendName);
    }
}
public class p283 {
    public static void main(String[] args) {
        File dirFile=new File("src/.");
        FileAccept fileAccept=new FileAccept();
        fileAccept.setExtendName("java" );
        String fileName[]=dirFile.list(fileAccept);
        for(String name:fileName){
            System.out.println(name);
        }
    }
}
