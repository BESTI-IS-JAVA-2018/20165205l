/**
 * Created by Administrator on 2018/3/28 0028.
 */
import java.io.*;
public class p286 {
    public static void main(String[] args) {
        int n = -1;
        byte[] a = new byte[100];
        try {
            File f = new File("p283.java");
            InputStream in = new FileInputStream(f);
            while ((n = in.read(a, 0, 100) )!= -1) {
                String s = new String(a, 0, n);
                System.out.print(s);
            }
            in.close();
        }
        catch (IOException e) {
            System.out.println("File read Error" + e);
        }
    }
}