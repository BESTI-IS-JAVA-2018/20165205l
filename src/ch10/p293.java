/**
 * Created by Administrator on 2018/3/28 0028.
 */
import java.io.*;
public class p293 {
    public static void main(String[] args) {
        RandomAccessFile inAndOut=null;
        int date []={1,2,3,4,5,6,7,8,9,10};
        try{
            inAndOut=new RandomAccessFile("tom.dat","rw");
            for(int i=0;i<date.length;i++) {
                inAndOut.writeInt(date[i]);
            }
            for(long i=date.length-1;i>=0;i--){
                inAndOut.seek(i*4);
                System.out.printf("\t%d",inAndOut.readInt());
            }
            inAndOut.close();
        }
        catch (IOException e){}
    }
}
