abstract class SIM {
	public abstract void setNumber(String n);
	public abstract String giveNumber();
	public abstract String giveCorpName();
	}
class MobileTelephone {
SIM card;
public void useSIM(SIM card) {
this.card=card;
	}
public void showMess() {
	System.out.println("使用的卡号是："+card.giveCorpName()+"提供的");
	System.out.println("手机号码是："+card.giveNumber());
	}
}
class SIMOfChinaMobile extends SIM{
	String number;
	public void setNumber (String n) {
	number=n;
	}
public String giveNumber() {
	return number;
	}
public String giveCorpName() {
	return "中国移动";
	}
} 
class SIMOfChinaUnicom extends SIM{
	String number;
	public void setNumber (String n) {
	number=n;}
public String giveNumber() {
	return number;
}
public String giveCorpName() {
	return "中国联通";
	}
}
public class p136 {
public static void main (String args[]) {
	MobileTelephone telephone=new MobileTelephone();
	SIM sim=new SIMOfChinaMobile();
	sim.setNumber("188****8***");
	telephone.useSIM(sim);
	telephone.showMess();
	 sim=new SIMOfChinaUnicom();
	sim.setNumber("13097656437");
	telephone.useSIM(sim);
	telephone.showMess();
	}
}
