/**
 * Created by Administrator on 2018/4/8 0008.
 */
class CPU{
    int speed;
    int getSpeed(){
        return speed;
    }
    CPU(int speed) {
        this.speed=speed;
    }
    public String toString(){
        String str=super.toString();
        return str;
    }
    void equals(){}
}
class HardDisk {
    int amount;
    int getAmount() {
        return amount;
    }
    HardDisk(int amount) {
        this.amount=amount;
    }
    public String toString() {
        String str = super.toString();
        return str;
    }
    void equals(){}
}
class PC {
    CPU cpu;
    HardDisk HD;
    PC(CPU cpu) {
        this.cpu = cpu;
    }
    PC(HardDisk HD) {
        this.HD = HD;
    }
    PC(CPU cpu, HardDisk HD) {
        System.out.println("cpu's speed:" + cpu.getSpeed());
        System.out.println("HD rl:" + HD.getAmount());
    }
    public String toString() {
        String str = super.toString();
        return str;
    }
    void equals(){}
}

public class Text {
    public static void main(String args[]) {
        CPU cpu;
        cpu=new CPU(2200);
        HardDisk HD;
        HD=new HardDisk(200);
        PC pc;
        pc=new PC(cpu);
        pc=new PC(HD);
        pc=new PC(cpu,HD);
        System.out.println(cpu.toString());
        System.out.println(HD.toString());
        System.out.println(pc.toString());
        System.out.println(cpu.equals(cpu));
        System.out.println(cpu.equals(HD));
        System.out.println(cpu.equals(pc));
        System.out.println(HD.equals(HD));
        System.out.println(HD.equals(pc));
        System.out.println(pc.equals(pc));
    }
}
