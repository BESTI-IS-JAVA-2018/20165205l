import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class Arithmetic4Test {
        ArithmeticFunc calc = new ArithmeticFunc();
        @Before
        public void setUp() throws Exception {
        }

        @Test
        public void testCalc() {

        }

        @Test
        public void testSqrt() {
            calc.sqrt("25");
            assertEquals("5.0", calc.getResult());
        }

        @Test
        public void testPow() {
            calc.pow("2^3");
            assertEquals("8.0" , calc.getResult());
            calc.pow("1/2^3");
            assertEquals("1/8" , calc.getResult());

        }

        @Test
        public void testAdd() {
            calc.add("1+2");
            assertEquals("3" , calc.getResult());
            calc.add("1/2+1/3");
            assertEquals("5/6" , calc.getResult());
        }

        @Test
        public void testSubstract() {
            calc.substract("3-1");
            assertEquals("2", calc.getResult());
            calc.substract("1/2-1/3");
            assertEquals("1/6", calc.getResult());

        }

        @Test
        public void testMultiply() {
            calc.multiply("2*3");
            assertEquals("6" , calc.getResult());
            calc.multiply("1/2*1/3");
            assertEquals("1/6" , calc.getResult());
        }

    @Test
    public void testDivide() {
        calc.divide("8÷4");
        assertEquals("2", calc.getResult());

    }
}

