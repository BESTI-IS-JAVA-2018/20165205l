import java.sql.*;
import java.util.*;
public class Example11_2 {
   public static void main(String args[]) {
      Connection con;
      Statement sql; 
      ResultSet rs;
      con = GetDBConnection.connectDB("students","root","");
      if(con == null ) return;
      try { 
          sql=con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                                    ResultSet.CONCUR_READ_ONLY);
          rs = sql.executeQuery("SELECT * FROM mess ");
          rs.last();
          int max = rs.getRow();
          System.out.println("表共有"+max+"条记录，随机抽取两条");
          int [] a =GetRandomNumber.getRandomNumber(max,2);
          for(int i:a){
             rs.absolute(i);
             String number = rs.getString(1);
             String name = rs.getString(2);
             Date date = rs.getDate(3);
             float h = rs.getFloat(4);
             System.out.printf("%s\t",number);
             System.out.printf("%s\t",name);
             System.out.printf("%s\t",date); 
             System.out.printf("%.2f\n",h);
          }
          con.close();
      }
      catch(SQLException e) { 
         System.out.println(e);
      }
  }
}

class GetRandomNumber {
    public static int [] getRandomNumber(int max,int amount)
    int [] randomNumber = new int[amount];
    int index =0;
    randomNumber[0]= -1;
    Random random = new Random();
     while(index<amount){
        int number = random.nextInt(max)+1;
        boolean isInArrays=false;
        for(int m:randomNumber){
            if(m == number)
                isInArrays=true;
        }
        if(isInArrays==false){
            randomNumber[index] = number;
            index++;
        }
    }
     return  randomNumber;
}
class GetDBConnection {
    public static Connection connectDB(String DBName,String id,String p) {
        Connection con = null;
        String
                uri = "jdbc:mysql://localhost:3306/"+DBName+"?useSSL=true&characterEncoding=utf-8";
        try{  Class.forName("com.mysql.jdbc.Driver");
        }
        catch(Exception e){}
        try{
            con = DriverManager.getConnection(uri,id,p);
        }
        catch(SQLException e){}
        return con;
    }
}
