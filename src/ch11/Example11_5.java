import java.sql.*; 
public class Example11_5 {
   public static void main(String args[]) {
      Connection con;
      PreparedStatement preSql;
      ResultSet rs;
      con = GetDBConnection.connectDB("students","root","");
      if(con == null ) return;
      String sqlStr ="insert into mess values(?,?,?,?)";
      try { 
          preSql = con.prepareStatement(sqlStr);
          preSql.setString(1,"A001");
          preSql.setString(2,"刘伟");
          preSql.setString(3,"1999-9-10");
          preSql.setFloat(4,1.77f);
          int ok = preSql.executeUpdate();
          sqlStr="select * from mess where name like ? ";
          preSql = con.prepareStatement(sqlStr);
          preSql.setString(1,"张%");
          rs = preSql.executeQuery();
          while(rs.next()) { 
             String number=rs.getString(1);
             String name=rs.getString(2);
             Date date=rs.getDate(3);
             float height=rs.getFloat(4);
             System.out.printf("%s\t",number);
             System.out.printf("%s\t",name);
             System.out.printf("%s\t",date); 
             System.out.printf("%.2f\n",height);
          }
          con.close();
      }
      catch(SQLException e) { 
         System.out.println("记录中number值不能重复"+e);
      }
  }
}
import java.sql.*;
class GetDBConnection {
    public static Connection connectDB(String DBName,String id,String p) {
        Connection con = null;
        String uri =
                "jdbc:mysql://localhost:3306/"+DBName+"?useSSL=true&characterEncoding=utf-8";
        try{  Class.forName("com.mysql.jdbc.Driver");
        }
        catch(Exception e){}
        try{
            con = DriverManager.getConnection(uri,id,p);
        }
        catch(SQLException e){}
        return con;
    }
}
