interface SpeakHello{
	void speakHello();
	}
class Chinese implements SpeakHello {
	public void speakHello() {
		System.out.println("中国人习惯用语：你好，吃饭了吗？");
	}
}
class English implements SpeakHello {
	public void speakHello() {
		System.out.println("外国人人习惯用语：天气不差？");
	}
}
class KindHello {
	public void lookHello(SpeakHello hello) {
	hello.speakHello();
	}
}
public class p154 {
public static void main(String args[]) {
	KindHello kindHello=new KindHello();
	kindHello.lookHello(new Chinese());
	kindHello.lookHello(new English());
	}
}
