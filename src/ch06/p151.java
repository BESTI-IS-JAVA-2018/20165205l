abstract class MotorVehicles {
abstract void brake();
}
interface MoneyFare {
	void charge();
}
interface ControlTemperature {
	void controlAirTemperature ();
}
class Bus extends MotorVehicles implements MoneyFare {
	void brake() {
	System.out.println("公交车使用刹车");
	}
	public void charge() {
	System.out.println("车票医院一张");
	}
}
class Taxi extends MotorVehicles implements MoneyFare,
ControlTemperature {
void brake () {
	System.out.println("出租车使用盘式刹车技术");
	}
	public void charge() {
	System.out.println("出租车2/公里，起价3公里");
}
public void controlAirTemperature() {
	System.out.println("出租车安装了空调");
	}
}
class Cinema implements MoneyFare,ControlTemperature {
	public void charge() {
	System.out.println("电影院：门票，10元/张");
}
public void controlAirTemperature() {
	System.out.println("电影院安装了空调");
	}
}
public class p151 {
public static void main (String args[]) {
	Bus bus101=new Bus();
	Taxi buleTaxi=new Taxi();
	Cinema redStarCinema=new Cinema();
	MoneyFare fare;
	ControlTemperature temperature;
	fare=bus101;
	bus101.brake();
	fare.charge();
	fare=buleTaxi;
	temperature=buleTaxi;
	buleTaxi.brake();
	fare.charge();
	temperature.controlAirTemperature();
	fare=redStarCinema;
	temperature=redStarCinema;
	fare.charge();
	temperature.controlAirTemperature();
	}
}
