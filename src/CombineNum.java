/**
 * @author zhangyuxin
 */
public class CombineNum {
    public static void main(String args[]) {
        int[] m = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            m[i] = Integer.parseInt(args[i]);
        }
        System.out.println(count(m[0], m[1]));
    }

    public static int count(int n, int m) {
        int temp;
        if (n < m) {
            temp = m;
            m = n;
            n = temp;
        } //若第一个数小于第二个数，则进行值的交换
        if (m == 0||m == n) {
            return 1;
        }
        else if (m == 1){
            return n;
        }
        else {
            return count(n - 1, m - 1) + count(n - 1, m);
        }
    }
}


