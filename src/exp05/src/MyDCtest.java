import java.util.Scanner;

public class MyDCtest {
    public static void main(String [] args) {
        String expression;
        int result;
        try
        {
            Scanner in = new Scanner(System.in);
            MyDC evaluator = new MyDC();
            System.out.println ("Enter a valid infix expression: ");
            expression = in.nextLine();
            String postfix = MyBC.toPostfix(expression);
            System.out.println ("The postfix expression is :" + postfix);
            result = evaluator.value (postfix);
            System.out.println ("That expression equals :" + result);
        }
        catch (Exception IOException)
        {
            System.out.println("Input exception reported");
        }
    }
}