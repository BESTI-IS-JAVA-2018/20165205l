import org.junit.Test;
import static org.junit.Assert.*;
public class MyComplexTest {
    MyComplex a=new MyComplex(1,2);
    MyComplex b=new MyComplex(2,-4);
    MyComplex c=new MyComplex(19,0);
    MyComplex d=new MyComplex(0,-3);
    MyComplex e=new MyComplex(0,0);
    @Test
    public void getRealPart() throws Exception {
        assertEquals(1, MyComplex.getRealPart(1));
        assertEquals(-1, MyComplex.getRealPart(-1));
        assertEquals(5, MyComplex.getRealPart(5));
        assertEquals(22, MyComplex.getRealPart(22));
        assertEquals(-100, MyComplex.getRealPart(-100));
        assertEquals(0, MyComplex.getRealPart(0));
    }
    @Test
    public void getImaginePart() throws Exception {
        assertEquals(1, MyComplex.getImaginePart(1));
        assertEquals(-1, MyComplex.getImaginePart(-1));
        assertEquals(5, MyComplex.getImaginePart(5));
        assertEquals(22, MyComplex.getImaginePart(22));
        assertEquals(-100, MyComplex.getImaginePart(-100));
        assertEquals(0, MyComplex.getImaginePart(0));
    }
    @Test
    public void add() throws Exception {
        assertEquals("(2.0-2.0i)", a.add(b).toString());
        assertEquals("(20.0+2.0i)", a.add(c).toString());
        assertEquals("(1.0-1.0i)", a.add(d).toString());
        assertEquals("(1.0+2.0i)", a.add(e).toString());
    }
    @Test
    public void minus() throws Exception {
        assertEquals("(0.0+6.0i)", a.minus(b).toString());
        assertEquals("(-18.0+2.0i)", a.minus(c).toString());
        assertEquals("(1.0+5.0i)", a.minus(d).toString());
        assertEquals("(1.0+2.0i)", a.minus(e).toString());
    }
    @Test
    public void multiply() throws Exception {
        assertEquals("(9.0-2.0i)", a.multiply(b).toString());
        assertEquals("(19.0+38.0i)", a.multiply(c).toString());
        assertEquals("(6.0-3.0i)", a.multiply(d).toString());
        assertEquals("(0.0)", a.multiply(e).toString());
    }
}