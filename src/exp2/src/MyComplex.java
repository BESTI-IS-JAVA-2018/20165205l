import java.util.Scanner;
public class MyComplex {
    static int r;
    static int i;
    private double m;
    private double n;
    public static int getRealPart(int RealPart){
        r = RealPart;
        return r;
    }
    public static int getImaginePart(int ImaginePart){
        i = ImaginePart;
        return i;
    }
    public MyComplex(double m, double n) {
        this.m = m;
        this.n = n;
    }
    public MyComplex add(MyComplex c) {
        return new MyComplex(m + c.m, n + c.n);
    }
    public MyComplex minus(MyComplex c) {
        return new MyComplex(m - c.m, n - c.n);
    }
    public MyComplex multiply(MyComplex c) {
        return new MyComplex(m * c.m - n * c.n, m * c.n + n * c.m);
    }
    public String toString() {
        String rtr_str = "";
        if (n > 0)
            rtr_str = "(" + m + "+" + n + "i" + ")";
        if (n == 0)
            rtr_str = "(" + m + ")";
        if (n < 0)
            rtr_str = "(" + m + n + "i" + ")";
        return rtr_str;
    }
}