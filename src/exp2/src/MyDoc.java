abstract class Data{
    public abstract void DisplayValue();
}
class Integer extends Data {
    int value;
    Integer(){
        value=100;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}
class Document {
    Data pd;
    Document() {
        pd=new Integer();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
public class MyDoc {
    static Double d;
    public static void main(String[] args) {
        d = new Double();
        d.DisplayValue();
    }
}
class Double extends Data{
    double mc;
    Double(){
        mc=1234567890.987654321123456789765;
    }
    public void DisplayValue(){
        System.out.println(mc);
    }
}