class Circle {
	double radius,area;
	void setRadius (double r) {
	radius=r;
	}
double getRadius () {
	return radius;
	}
double getArea() {
	area=3.14*radius*radius;
	return area;
	}
}
class Circular {
	Circle bottom;
	double height;
	void setBottom(Circle c) {
	bottom=c;
	}
	void setHeight(double h) {
	height=h;
	}
	double getVolme() {
	if (bottom==null)
		return -1;
	else
		return bottom.getArea()*height/3.0;
	}
	double getBottomRadius() {
	return bottom.getRadius();
	}
	public void setBottomRadius(double r) {
	bottom.setRadius(r);
	}
}
public class p72 {
public static void main (String args[]) {
	Circle circle =new Circle();
	circle.setRadius(10);
	Circular circular=new Circular();
	System.out.println("circle的引用:"+circle);
	circular.setHeight(5);
	circular.setBottom(circle);
	System.out.println("circle的引用:"+circle);
	System.out.println("圆锥的bottom的引用:"+circular.bottom);
	System.out.println("圆锥的体积:"+circular.getVolme());
	circle.setRadius(20);
	System.out.println("bottom的半径:"+circular.getBottomRadius());
	System.out.println("重新创建circle，引用发生变化");
	circle=new Circle();
	System.out.println("circle的引用："+circle);
	System.out.println("bottom不变");
	System.out.println("圆锥的bottom的引用:"+circular.bottom);
	}
}
